#####
# Copyright 2018 ONERA
#
# This file is part of the MAUVE ARIA project.
#
# MAUVE ARIA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# MAUVE ARIA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
cmake_minimum_required(VERSION 2.8.3)
project(aria_mauve)

## C++ 11
set ( CMAKE_CXX_STANDARD 11 )
set ( CMAKE_CXX_EXTENSIONS False )

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
 mauve_runtime
 mauve_types
 mauve_base_components
)

## System dependencies are found with CMake's conventions
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
find_package(ARIA REQUIRED)
include_directories(${ARIA_INCLUDE_DIR})

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS include
  LIBRARIES aria_mauve
  CATKIN_DEPENDS mauve_runtime mauve_types
  DEPENDS ARIA
)

###########
## Build ##
###########

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ library with your components and architectures
set (MAUVE_ARIA_SRC
  src/AriaWrapper.cpp
  src/AriaComponent.cpp
)
add_library ( aria_mauve ${MAUVE_ARIA_SRC} )
target_link_libraries ( aria_mauve ${catkin_LIBRARIES} ${ARIA_LIBRARY} )

## Declare a C++ executable, e.g. for a test file
add_executable ( aria_mauve_component tests/test.cpp )
target_link_libraries ( aria_mauve_component aria_mauve )

## Declare a C++ library with your python deployer
add_library ( aria_mauve_component_py tests/test_component.cpp )
target_link_libraries ( aria_mauve_component_py aria_mauve )
