/* Copyright 2018 ONERA
 *
 * This file is part of the MAUVE ARIA project.
 *
 * MAUVE ARIA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ARIA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <math.h>
#include <Aria.h>

#include <mauve/runtime.hpp>
#include "aria/mauve/AriaComponent.hpp"

namespace aria {
  namespace mauve {

    bool AriaCore::configure_hook() {
      init();
      bool ok = connect(this->device, this->baudrate)
        && startRobot();
      if (!ok) return false;
      logRobotInformation();
      return true;
    }

    void AriaCore::cleanup_hook() {
      stopRobot();
      disconnect();
      uninit();
    }

    void AriaCore::update() {
      // Write states
      shell().pose.write(getPose());
      shell().velocity.write(getVelocity());
      shell().voltage.write(getBatteryVoltage());
      double t = getTime();
      shell().time.write(t);
      logger().debug("ARIA time {:03.4f}", t);
      // Read commands
      auto data = shell().vel_cmd.read();
      if (data.status == ::mauve::runtime::DataStatus::NEW_DATA) {
        logger().debug("received velocity command {}", data.value);
        sendUnicycleVelocityCommand(data.value);
      }
    }

    bool AriaFSM::configure_hook() {
      period = ::mauve::runtime::ms_to_ns(core().cycle());
      logger().debug("period set to {} ns", period);
      return ::mauve::base::PeriodicStateMachine<AriaShell,
        AriaCore>::configure_hook();
    }

}} // namespace
