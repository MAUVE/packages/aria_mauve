/* Copyright 2018 ONERA
 *
 * This file is part of the MAUVE ARIA project.
 *
 * MAUVE ARIA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ARIA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <math.h>
#include <Aria.h>

#include <mauve/runtime.hpp>
#include "aria/mauve/AriaWrapper.hpp"

namespace aria {
namespace mauve {

AriaWrapper::AriaWrapper() {}

unsigned int AriaWrapper::cycle() const {
  return robot->getCycleTime();
}

bool AriaWrapper::logRobotInformation() {
  if (! robot) return false;
  logger().debug("robot name: {}", robot->getRobotName());
  logger().debug("robot type: {}", robot->getRobotType());
  logger().debug("absolute max lat accel: {} m/s^2", robot->getAbsoluteMaxLatAccel()*0.001);
  logger().debug("absolute max lat decel: {} m/s^2", robot->getAbsoluteMaxLatDecel()*0.001);
  logger().debug("absolute max lat vel: {} m/s",   robot->getAbsoluteMaxLatVel()*0.001);
  logger().debug("absolute max rot accel: {} rad/s^2", robot->getAbsoluteMaxRotAccel() * M_PI/180);
  logger().debug("absolute max rot decel: {} rad/s^2", robot->getAbsoluteMaxRotDecel()* M_PI/180);
  logger().debug("absolute max rot vel: {} rad/s",   robot->getAbsoluteMaxRotVel()* M_PI/180);
  logger().debug("absolute max trans accel: {} m/s^2", robot->getAbsoluteMaxTransAccel()*0.001);
  logger().debug("absolute max trans decel: {} m/s^2", robot->getAbsoluteMaxTransDecel()*0.001);
  logger().debug("absolute max trans vel: {} m/s",   robot->getAbsoluteMaxTransVel()*0.001);
  logger().debug("absolute max trans neg vel: {} m/s", robot->getAbsoluteMaxTransNegVel()*0.001);
  std::string charge_status;
  switch (robot->getChargeState()) {
    case ArRobot::CHARGING_UNKNOWN: charge_status = "UNKNOWN"; break;
    case ArRobot::CHARGING_NOT: charge_status = "NOT"; break;
    case ArRobot::CHARGING_BULK: charge_status = "BULK"; break;
    case ArRobot::CHARGING_FLOAT: charge_status = "FLOAT"; break;
    case ArRobot::CHARGING_OVERCHARGE: charge_status = "OVERCHARGE"; break;
    case ArRobot::CHARGING_BALANCE: charge_status = "BALANCE"; break;
  }
  logger().debug("charge status: {}", charge_status);
  logger().debug("charge state: {}", robot->getStateOfCharge());
  logger().debug("battery voltage: {} ({} / 12V)",
    robot->getRealBatteryVoltage(), robot->getBatteryVoltage());
  logger().debug("temperature: {}", robot->getTemperature());
  logger().debug("cycle time: {} ms", robot->getCycleTime());
  logger().debug("initial compass angle: {}", robot->getCompass());
  return true;
}

bool AriaWrapper::init() {
  logger().info("Initializing ARIA");
  ::Aria::init(::Aria::SIGHANDLE_NONE);
  parser = new ArArgumentParser(new ArArgumentBuilder());
  connector = new ArRobotConnector(nullptr, nullptr, false, false);
  robot = new ArRobot();
  return true;
}

bool AriaWrapper::connect(const std::string &device, int baudrate) {
	std::stringstream ss;
  ss << "-robotPort " << device
    << " -robotBaud " << baudrate;
	parser->addDefaultArgument(ss.str().c_str());
  connector->parseArgs(parser);
  logger().debug("parsing connection arguments");
  if (! connector->connectRobot(robot)) {
    logger().error("Cannot connect to device {}", device);
    return false;
  }
  if (!robot->isConnected())  {
    // should be true
    logger().error("Internal error: robot connector succeeded but ArRobot::isConnected() is false!");
    return false;
  }
  logger().info("Connected to robot {}", robot->getRobotName());
  return true;
}

bool AriaWrapper::startRobot() {
  // start
  robot->runAsync(true);
  robot->lock();
  robot->disableSonar();
  bool enable_motors = robot->comInt(ArCommands::ENABLE, 1);
  robot->stop();
  robot->unlock();
  if (! enable_motors) {
    logger().error("Could not turn robot motors on");
    return false;
  }
  logger().info("Robot {} started", robot->getRobotName());
  return true;
}

bool AriaWrapper::stopRobot() {
  robot->lock();
  robot->stop();
  robot->unlock();
  logger().info("Robot {} stopped", robot->getRobotName());
  return true;
}

bool AriaWrapper::disconnect() {
  robot->stopRunning(false);
  robot->disconnect();
  logger().info("Disconnected from robot {}", robot->getRobotName());
  return true;
}

bool AriaWrapper::uninit() {
  ::Aria::uninit();
  delete robot;
  delete connector;
  delete parser;
  return true;
}

using namespace ::mauve::types::geometry;

Point2D AriaWrapper::getLocation() {
  robot->lock();
  double x = robot->getEncoderX() * 0.001;
  double y = robot->getEncoderY() * 0.001;
  robot->unlock();
  return { x , y };
}

double AriaWrapper::getHeading() {
  robot->lock();
  double th = robot->getEncoderTh() * M_PI/180;
  robot->unlock();
  return th;
}

Pose2D AriaWrapper::getPose() {
  return {
    getLocation(),
    getHeading()
  };
}

UnicycleVelocity AriaWrapper::getVelocity() {
  robot->lock();
  double v = robot->getVel() * 0.001;
  double w = robot->getRotVel() * M_PI/180;
  robot->unlock();
  return { v , w };
}

double AriaWrapper::getBatteryVoltage() {
  robot->lock();
  double v;
  if (robot->hasStateOfCHarge())
    v = robot->getStateOfCharge();
  else
    v = robot->getBatteryVoltage();
  robot->unlock();
  return v;
}

bool AriaWrapper::sendUnicycleVelocityCommand(const UnicycleVelocity& cmd) {
  robot->lock();
  robot->setVel(cmd.linear * 1000);
  robot->setRotVel(cmd.angular * 180 / M_PI);
  robot->unlock();
  return true;
}

double AriaWrapper::getTime() {
  robot->lock();
  ArTime t = robot->getLastOdometryTime();
  robot->unlock();
  return t.getSec() + ( t.getMSec() * 0.001 );
}

}} // namespace
