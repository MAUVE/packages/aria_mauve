/* Copyright 2018 ONERA
 *
 * This file is part of the MAUVE ARIA project.
 *
 * MAUVE ARIA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ARIA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#include <math.h>
#include <Aria.h>

#include "test.hpp"

int main() {

  using namespace aria::mauve;

  std::stringstream config;
  config << "default:" << std::endl;
  config << "  type: stdout" << std::endl;
  config << "  level: trace" << std::endl;
  mauve::runtime::AbstractLogger::initialize(config);

  auto archi = new AriaComponentArchitecture();

  mk_deployer(archi);
  auto depl = mauve::runtime::AbstractDeployer::instance();

  archi->configure();
  depl->create_tasks();
  depl->activate();
  depl->start();

  depl->loop();

  depl->stop();
  depl->clear_tasks();
  archi->cleanup();

  return 0;
}
