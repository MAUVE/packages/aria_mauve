/* Copyright 2018 ONERA
 *
 * This file is part of the MAUVE ARIA project.
 *
 * MAUVE ARIA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ARIA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_ARIA_ARIARESOURCE_HPP
#define MAUVE_ARIA_ARIARESOURCE_HPP

#include <mauve/runtime.hpp>
#include <mauve/types/geometry_types.hpp>
#include "AriaComponent.hpp"

namespace aria {
  namespace mauve {

    /** ARIA Resource Inteface */
    struct AriaInterface : public ::mauve::runtime::Interface<AriaShell, AriaCore> {
      /** Stop service to stop robot motion. */
      ::mauve::runtime::EventService & stop = this->mk_event_service("stop",
        &AriaCore::stopRobot);
      /** Read service to read location */
      ::mauve::runtime::ReadService<::mauve::types::geometry::Point2D> & read_location =
        this->mk_read_service<::mauve::types::geometry::Point2D>("read_location",
          &AriaCore::getLocation);
      /** Read service to read pose */
      ::mauve::runtime::ReadService<::mauve::types::geometry::Pose2D> & read_pose =
        this->mk_read_service<::mauve::types::geometry::Pose2D>("read_pose",
          &AriaCore::getPose);
      /** Read service to read velocity */
      ::mauve::runtime::ReadService<::mauve::types::geometry::UnicycleVelocity> & read_velocity =
        this->mk_read_service<::mauve::types::geometry::UnicycleVelocity>("read_velocity",
          &AriaCore::getVelocity);
      /** Read service to read heading */
      ::mauve::runtime::ReadService<double> & read_heading =
        this->mk_read_service<double>("read_heading",
          &AriaCore::getHeading);
      /** Read service to read battery voltage */
      ::mauve::runtime::ReadService<double> & read_battery =
        this->mk_read_service<double>("read_battery",
          &AriaCore::getBatteryVoltage);
      /** Read service to read odometry time */
      ::mauve::runtime::ReadService<double> & read_time =
        this->mk_read_service<double>("read_time",
          &AriaCore::getTime);
      /** Write service to send velocity command */
      ::mauve::runtime::WriteService<::mauve::types::geometry::UnicycleVelocity> & write_velocity =
        this->mk_write_service<::mauve::types::geometry::UnicycleVelocity>("write_velocity",
          &AriaCore::sendUnicycleVelocityCommand);

    }; // AriaInterface

    using AriaResource = ::mauve::runtime::Resource<AriaShell, AriaCore, AriaInterface>;

}} // namespace

#endif // MAUVE_ARIA_ARIARESOURCE_HPP
