/* Copyright 2018 ONERA
 *
 * This file is part of the MAUVE ARIA project.
 *
 * MAUVE ARIA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ARIA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_ARIA_ARIAWRAPPER_HPP
#define MAUVE_ARIA_ARIAWRAPPER_HPP

#include <string>

#include <mauve/runtime.hpp>
#include <mauve/types/geometry_types.hpp>

class ArRobot;
class ArRobotConnector;
class ArArgumentParser;

namespace aria {
  namespace mauve {

    /** Wrapper for the ARIA library to access the P3DX robot */
    class AriaWrapper : virtual public ::mauve::runtime::WithLogger {
    public:
      AriaWrapper();

      /** Init the structures */
      bool init();
      /** Uninit the structures */
      bool uninit();

      /**
       * Connect to the robot
       * @param device Robot port device (something like /dev/ttyUSB0)
       * @param baudrate Baudrate of the connection
       * @return connection success
       */
      bool connect(const std::string& device, int baudrate);
      /** Enable motors */
      bool startRobot();
      /** Stop the robot motion */
      bool stopRobot();
      /** Diconnect from the robot */
      bool disconnect();
      /** Log information about the platform */
      bool logRobotInformation();

      /** Get robot location.
       * @return x,y coordinates of the robot from odometry
       */
      ::mauve::types::geometry::Point2D getLocation();
      /** Get robot psoe.
       * @return location and heading
       */
      ::mauve::types::geometry::Pose2D getPose();
      /** Get robot heading.
       * @return heading from odometry
       */
      double getHeading();
      /** Get robot velocity.
       * @return linear and angular velocities
       */
      ::mauve::types::geometry::UnicycleVelocity getVelocity();
      /** Get battery voltage.
       * @return voltage
       */
      double getBatteryVoltage();
      /** Get robot time.
       * @return odometry time in seconds
       */
      double getTime();

      /** Send a command to the robot.
       * @param cmd linear and angular velocities command.
       * @return true
       */
      bool sendUnicycleVelocityCommand(const ::mauve::types::geometry::UnicycleVelocity& cmd);

      /** Get cycle time
       * @return duration of the connection cycle
       */
      unsigned int cycle() const;

    private:
      /** Pointer to the robot handler */
      ArRobot* robot;
      /** Pointer to the conenction handler */
      ArRobotConnector* connector;
      /** Pointer to the argumets of the connection */
      ArArgumentParser* parser;
    };

  }
}

#endif // MAUVE_ARIA_ARIAWRAPPER_HPP
