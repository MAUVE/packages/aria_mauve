/* Copyright 2018 ONERA
 *
 * This file is part of the MAUVE ARIA project.
 *
 * MAUVE ARIA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE ARIA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */
#ifndef MAUVE_ARIA_ARIACOMPONENT_HPP
#define MAUVE_ARIA_ARIACOMPONENT_HPP

#include <mauve/runtime.hpp>
#include <mauve/types/geometry_types.hpp>
#include <mauve/base/PeriodicStateMachine.hpp>
#include "AriaWrapper.hpp"

namespace aria {
  namespace mauve {

    /** ARIA Shell */
    struct AriaShell : public ::mauve::runtime::Shell {
      /* Output port for robot pose */
      ::mauve::runtime::WritePort<::mauve::types::geometry::Pose2D>& pose =
        mk_write_port<::mauve::types::geometry::Pose2D>("pose");
      /** Output port for robot velocity */
      ::mauve::runtime::WritePort<::mauve::types::geometry::UnicycleVelocity>& velocity =
        mk_write_port<::mauve::types::geometry::UnicycleVelocity>("velocity");
      /** Output port for battery voltage */
      ::mauve::runtime::WritePort<double>& voltage = mk_write_port<double>("battery");
      /** Output port for robot time */
      ::mauve::runtime::WritePort<double>& time = mk_write_port<double>("time");
      /** Input port for command */
      ::mauve::runtime::ReadPort< ::mauve::runtime::StatusValue<::mauve::types::geometry::UnicycleVelocity> > & vel_cmd =
        mk_read_port<::mauve::runtime::StatusValue<::mauve::types::geometry::UnicycleVelocity>>(
          "velocity_command",
          ::mauve::runtime::StatusValue<::mauve::types::geometry::UnicycleVelocity> {
            ::mauve::runtime::DataStatus::NO_DATA,
            ::mauve::types::geometry::UnicycleVelocity{0,0} });
    }; // AriaShell

    /** ARIA Code. Inherits from the Wrapper. */
    struct AriaCore : public ::mauve::runtime::Core<AriaShell>, public AriaWrapper {
    public:
      /** Property for device name */
      ::mauve::runtime::Property<std::string>& device =
        mk_property<std::string>("device", "/dev/ttyUSB0");
      /** Property for connection baudrate */
      ::mauve::runtime::Property<unsigned int>& baudrate =
        mk_property<unsigned int>("baudrate", static_cast<unsigned int>(9600));
      /** Update function.
       * Apply input command if present.
       * Publish robot data to output ports.
       */
      void update();
    protected:
      /** Configuration.
       * Connects and starts
       */
      bool configure_hook() override;
      /** Cleaning.
       * Stops and disconnects.
       */
      void cleanup_hook() override;
    };

    /** ARIA FSM: a Periodic State Machine */
    struct AriaFSM : public ::mauve::base::PeriodicStateMachine<AriaShell, AriaCore> {
      virtual bool configure_hook() override;
    }; // AriaFSM

    using AriaComponent = ::mauve::runtime::Component<AriaShell, AriaCore, AriaFSM>;

}} // namespace

#endif // MAUVE_ARIA_ARIACOMPONENT_HPP
